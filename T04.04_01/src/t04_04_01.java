//T04_04_01

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class t04_04_01 {
	
	public static void copiar_archivo(String filename, String newfilename) {
		File origen = new File(filename);
		File destino = new File(newfilename);
		
		try {
			InputStream in = new FileInputStream(origen);
			OutputStream out = new FileOutputStream(destino);
			byte[] buffer = new byte[1024];
			int len;
			while ((len = in.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		if (args.length == 2) {
			copiar_archivo(args[0],args[1]);
		} else {
			System.out.println("necessary args: (String filename,String newfilename)");
		}
	}
		
}
