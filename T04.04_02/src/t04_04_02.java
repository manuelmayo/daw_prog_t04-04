//T04_04_02

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import java.io.FileReader;

public class t04_04_02 {
	
	public static void num_palabras_lineas(String filename) {
		File file = new File(filename);
		int lineas = 0;
		int palabras = 0;
		try {
			FileReader file_reader = new FileReader(file);
			BufferedReader buffered_reader = new BufferedReader(file_reader);
			String linea;
			while((linea=buffered_reader.readLine())!=null) {
				lineas++;
				String[] split_palabras = linea.split(" ");
				for (int i=0;i<split_palabras.length;i++) {
					if (!split_palabras[i].equals("")) {
						palabras++;
					}
				}
				//palabras += linea.split("\\S+").length;
			}
			System.out.printf("N�mero de L�neas: %s\nN�mero de Palabras: %s",
					lineas, palabras);
			file_reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public static void main(String[] args) {
		if (args.length == 1) {
			num_palabras_lineas(args[0]);
		}
	}
		
}
