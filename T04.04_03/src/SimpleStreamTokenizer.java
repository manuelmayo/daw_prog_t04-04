//T04_04_03

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;

public class SimpleStreamTokenizer {
	
	public static int TT_EOL = 0;
	public static int TT_WORD = 1;
	public FileReader file_reader;
	public BufferedReader buffered_reader;
	public boolean hai_separador = false;
	public char separador;
	
	public SimpleStreamTokenizer(FileReader fr) {
		this.file_reader = fr;
		this.buffered_reader = new BufferedReader(this.file_reader);
	}
	
	public SimpleStreamTokenizer(FileReader fr, char separador) {
		this.file_reader = fr;
		this.separador = separador;
		this.hai_separador = true;
		this.buffered_reader = new BufferedReader(this.file_reader);
	}
	
	public int nextToken() {
		try {
			if (this.buffered_reader.ready()) {
				return TT_WORD;
			} else {
				return TT_EOL;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public String sval() {
		try {
			if (hai_separador) {
				char caracter;
				String token = "";
				while ((caracter = (char) this.buffered_reader.read())!=this.separador &&
						caracter != (char) -1) {
					token += Character.toString(caracter);
				}
				return token;
			} else {
				char caracter = (char) this.buffered_reader.read();
				String token = Character.toString(caracter);
				return token;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
		
}
