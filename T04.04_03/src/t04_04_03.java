//T04_04_03

import java.io.FileReader;
import java.io.IOException;


public class t04_04_03 {
	
	public static void main(String[] args) {
		if (args.length==1) {
			FileReader file_reader;
			try {
				file_reader = new FileReader(args[0]);
				char separador = ",".charAt(0);
				SimpleStreamTokenizer sst = new SimpleStreamTokenizer(file_reader,separador);
				String token;
				int ntoken = 1;
				while(sst.nextToken() == SimpleStreamTokenizer.TT_WORD) {
					token = sst.sval();
					System.out.printf("token %s: %s\n",ntoken,token);
					ntoken++;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
		
}
