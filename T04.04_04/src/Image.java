//T04_04_04

import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import java.awt.image.BufferedImage;
import java.io.File;

public class Image {
	
	public File image_file;
	public BufferedImage image_buffered;
	public ImageInputStream iis;
	public String type;
	public int width;
	public int height;
	
	public Image(File imagefile) {
		this.image_file = imagefile;
		try {
			this.image_buffered = ImageIO.read(this.image_file);
			this.iis = ImageIO.createImageInputStream(this.image_file);
			Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(iis);
			while (imageReaders.hasNext()) {
				ImageReader reader = (ImageReader) imageReaders.next();
				this.type = reader.getFormatName();
			}
			this.width = this.image_buffered.getWidth();
			this.height = this.image_buffered.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Count red pixel in a given image
	//https://stackoverflow.com/questions/43443309/count-red-pixel-in-a-given-image
	
	public int CountPixels(int[] rgb) {
		int count = 0;
		for (int x=0;x<this.width;x++) {
			for (int y=0;y<this.height;y++) {
				int rgbn = this.image_buffered.getRGB(x, y);
				int red   = (rgbn >>> 16) & 0xFF;
	            int green = (rgbn >>>  8) & 0xFF;
	            int blue  = (rgbn >>>  0) & 0xFF;
	            if (red == rgb[0] && green == rgb[1] && blue == rgb[2]) {
	            	count++;
	            }
			}
		}
		return count;
	}
	
	public int CountPixels(int red, int green, int blue) {
		int[] rgb_array = new int[] {red,green,blue};
		return CountPixels(rgb_array);
	}
	
}
