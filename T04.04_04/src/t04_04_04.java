//T04_04_04

import java.io.File;
import java.util.Arrays;

public class T04_04_04 {
	
	public static void main(String[] args) {
		File file;
		file = new File("img/file.png");
		Image imagen = new Image(file);
		System.out.printf("Tipo: %s\n",imagen.type);
		System.out.printf("Ancho: %s\n",imagen.width);
		System.out.printf("Alto: %s\n",imagen.height);
		int[] rgb_color = new int[] {255,255,255};
		System.out.printf("N�mero de pixeles del color RGB{%s}: %s\n",
				Arrays.toString(rgb_color),imagen.CountPixels(rgb_color));
			
	}
		
}
