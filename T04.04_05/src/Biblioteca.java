import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//T04_04_05

public class Biblioteca {
	
	public String file_name = "biblioteca.dat";
	public String file_temp_name = "bibliotecaTemp.dat";
	public File file_princ = new File(file_name);
	public File file_temp = new File(file_temp_name);
	public FileWriter file_writer;
	public BufferedWriter buffered_writer;
	public FileReader file_reader;
	public BufferedReader buffered_reader;
	
	public List<Libro> libros = new ArrayList<Libro>();
	
	public Biblioteca() throws IOException {
		this.resetear_dat();
	}
	
	public Biblioteca(List<Libro> libros) throws Exception {
		for (int i=0;i<libros.size();i++) {
			this.engadir(libros.get(i));
		}
		this.resetear_dat();
	}

	public void listado() {
		for (int i=0;i<this.libros.size();i++) {
			System.out.println(this.libros.get(i).toString());
		}
	}
	
	public void engadir(Libro libro) throws Exception {
		List<String> ids = new ArrayList<String>();
		for (int i=0;i<this.libros.size();i++) {
			ids.add(this.libros.get(i).id);
		}
		if (!ids.contains(libro.id)) {
			this.libros.add(libro);
			this.grabar_libro(libro);
		} else {
			throw new Exception("El ID ["+libro.id+"] ya existe");
		}
	}
	
	public void eliminar(String id) throws Exception {
		Libro libro = this.buscarID(id);
		this.libros.remove(libro);
		this.borrar_libro(id);
	}
	
	public Libro buscarID(String id) throws Exception {
		for (int i=0;i<this.libros.size();i++) {
			if (this.libros.get(i).id.equals(id)) {
				return this.libros.get(i);
			}
		}
		throw new Exception("No existe ning�n libro con esa ID");
	}
	
	public List<Libro> buscarTitulo(String titulo) {
		List<Libro> librosTitulo = new ArrayList<Libro>();
		for (int i=0;i<this.libros.size();i++) {
			if (this.libros.get(i).titulo == titulo) {
				librosTitulo.add(this.libros.get(i));
			}
		}
		return librosTitulo;
	}
	
	public List<Libro> buscarAutor(String autor) {
		List<Libro> librosAutor = new ArrayList<Libro>();
		for (int i=0;i<this.libros.size();i++) {
			if (this.libros.get(i).autor == autor) {
				librosAutor.add(this.libros.get(i));
			}
		}
		return librosAutor;
	}
	
	public List<Libro> buscarAno(int ano) {
		List<Libro> librosAno = new ArrayList<Libro>();
		for (int i=0;i<this.libros.size();i++) {
			if (this.libros.get(i).ano_edicion == ano) {
				librosAno.add(this.libros.get(i));
			}
		}
		return librosAno;
	}
	
	//M�todos FILE
	
	public void resetear_dat() throws IOException {
		this.file_writer =  new FileWriter(this.file_name);
		this.file_writer.close();
	}
	
	public void grabar_libro(Libro libro) throws IOException {
		this.file_writer =  new FileWriter(this.file_princ, true);
		this.buffered_writer = new BufferedWriter(file_writer);
		this.buffered_writer.append(libro.offset());
		this.buffered_writer.newLine();
		this.buffered_writer.close();
	}
	
	public void borrar_libro(String ID) throws IOException {
		this.file_temp = new File("bibliotecaTemp.dat");
		FileWriter temp_writer = new FileWriter(file_temp);
		BufferedWriter temp_buffered = new BufferedWriter(temp_writer);
		this.file_reader =  new FileReader(this.file_princ);
		this.buffered_reader = new BufferedReader(file_reader);
		String line;
		while((line = buffered_reader.readLine()) != null) {
			String[] string_libro = line.split(";");
			if (!string_libro[0].equals(ID)) {
				temp_buffered.write(line);
				temp_buffered.newLine();
			}
		}
		this.buffered_reader.close();
		temp_buffered.close();
		this.file_princ.delete();
		this.file_temp.renameTo(this.file_princ);
	}
	
}
