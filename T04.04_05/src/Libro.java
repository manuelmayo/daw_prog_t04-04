//T04_04_05

public class Libro {
	
	public String id;
	public String titulo;
	public String autor;
	public int ano_edicion;
	
	public Libro(String id,String titulo,String autor,int ano_edicion) {
		this.id = id;
		this.titulo = titulo;
		this.autor = autor;
		this.ano_edicion = ano_edicion;
	}
	
	public String offset() {
		return String.format("%s; %s; %s; %s",
				this.id,this.titulo,this.autor,this.ano_edicion);
	}
	
	public String toString() {
		return String.format("[%s]\n%-8s %s\n%-8s %s \n%-8s %s\n", 
				this.id,
				"Titulo:", this.titulo, 
				"Autor:", this.autor, 
				"A�o:", this.ano_edicion);
	}
	
}
