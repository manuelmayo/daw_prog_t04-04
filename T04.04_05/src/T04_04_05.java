//T04_04_05

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class T04_04_05 {
	
	public static void main(String[] args) throws Exception {
		
		 BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		Libro libro1 = new Libro(
				"A0001",
				"El coraz�n de la piedra",
				"Jos� Mar�a Garc�a L�pez",
				2013);
		Libro libro2 = new Libro(
				"A0002",
				"La polifon�a cl�sica",
				"Samuel Rubio",
				1956);
		Libro libro3 = new Libro(
				"A0003",
				"Mujeres en el agua",
				"Patricia Torres",
				2019);
		
		Biblioteca biblio = new Biblioteca();
		biblio.engadir(libro1);
		biblio.engadir(libro2);
		biblio.engadir(libro3);
		
		boolean bucle = true;
		
		while (bucle) {
			System.out.println(":::BIBLIOTECA:::");
			System.out.println("1) LISTAR");
			System.out.println("2) ENGADIR");
			System.out.println("3) ELIMINAR");
			System.out.println("4) SALIR");
			System.out.println();
			System.out.print("> ");
			String opt = reader.readLine();
			switch(opt) {
			case "1":
				biblio.listado();
				break;
			case "2":
				Libro newLibro;
				System.out.print("ID: ");
				String newId = reader.readLine();
				System.out.print("Titulo: ");
				String newTitulo = reader.readLine();
				System.out.print("Autor: ");
				String newAutor = reader.readLine();
				System.out.print("A�o Edici�n: ");
				int newAEdicion = Integer.parseInt(reader.readLine());
				newLibro = new Libro(newId, newTitulo, newAutor, newAEdicion);
				biblio.engadir(newLibro);
				System.out.println("<Libro engadido>");
				break;
			case "3":
				System.out.print("ID: ");
				String IDLibro = reader.readLine();
				biblio.eliminar(IDLibro);
				System.out.println("<Libro eliminado>");
				break;
			case "4":
				bucle = false;
				break;
			}
			System.out.println();
		}
		
	}
		
}
